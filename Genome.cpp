#include "Genome.hpp"
/**
 * @brief @brief Convert a char representation of a genomic base to int.
 *
 * @param base Base char 'A', 'C', 'G' or 'T'.
 * @return int8_t Int representation.
 */
int8_t base_to_int(char base)
{
    switch (std::toupper(base))
    {
        case 'A':
            return 0;
        case 'C':
            return 1;
        case 'G':
            return 2;
        case 'T':
            return 3;
        default:
            std::cerr << base << " is not a valid value for a genome base ('A', 'C', 'G' or 'T'), exiting." << std::endl;
            exit(1);
    }
}
/**
 * @brief @brief Converts an int representation of a genomic base to char.
 *
 * @param value Int value.
 * @return char Respective char.
 */
char int_to_base(int value)
{
    switch (value)
    {
    case 0:
        return 'A';
    case 1:
        return 'C';
    case 2:
        return 'G';
    case 3:
        return 'T';
    };
    return 'E';
}

/**
 * @brief Create a genome, i.e. a list of nucleotides.
 *
 * @param g String contaning the nucleotide sequence.
 */
Genome::Genome(char* genome)
{
    Nucleotide* n = new Nucleotide(0, nullptr);
    this->head = n;
    for(size_t i = 0; genome[i] != '\0'; i++)
        n = new Nucleotide(base_to_int(genome[i]), n);
    this->tail = n;
    this->genome_length = strlen(genome);
    this->mutation_number = 0;
}
/**
 * @brief Create a mutation in the desired position of the genome.
 *
 * @param pos Position of the mutation.
 * @param new_bas Character of the new nucleotide: A, T, C or G.
 * @param mutation_id Integer that represents the original sequence.
 * @return Index of the mutation.
 */
int Genome::MutatePosition(uint32_t pos, char new_base, int mutation_id=-1){
    // Instances a buffer to store sequence to copy
    Nucleotide* mutation = tail;
    if(mutation_list.count(mutation_id))
        mutation = mutation_list[mutation_id];
    int copy_length = mutation->GetPosition() - pos;
    if(copy_length < 0)
        std::cerr << "Trying to access an invalid position! Exiting..." << std::endl;
    uint8_t branch_seq[copy_length + 1]; // copy_length stores a position, so if copy_length = 0, the array should be of size 1
    // Iterate trough all position from n to the position to change
    Nucleotide* temp_n = mutation;
    size_t i = temp_n->GetPosition() - pos;
    // Store the value of current position on branch_seq
    while (temp_n->GetPosition() > (int)pos)
    {
        i = temp_n->GetPosition() - pos;
        branch_seq[i] = temp_n->GetValue();
        temp_n = temp_n->GetParent();
    }
    // Store value of new_base on branch_seq[0]
    branch_seq[0] = base_to_int(new_base);
    // Get the parent of the position to the change
    Nucleotide *parent = GetPosition(pos - 1);
    parent->SetHasMutation(true);
    // Iterate on the branch_seq creating childs where they dont exist
    for (i = 0; i <= (uint32_t) copy_length; i++)
    {
        // If child for the branch_seq[i] value does not exist create it
        if (!parent->GetChild((int)branch_seq[i])){
            Nucleotide* ng = new Nucleotide((int)branch_seq[i], parent);
            parent->SetChild(ng, (int)branch_seq[i]);
        }
        // Child becomes the parent for next value
        parent = parent->GetChild((int)branch_seq[i]);
    }
    mutation_number+=1;
    mutation_list.insert(std::pair<int, Nucleotide*> (mutation_number, parent));
    return mutation_number;
}
/**
 * @brief Print the genome and all of its mutations.
 *
 */ 
void Genome::PrintGenome(){
    std::map<int, Nucleotide*>::iterator it;
    std::cout << "Original sequence: ";
    PrintGenomeAux(tail, true);
    std::cout << "Mutations: " << std::endl;;
    for(it = mutation_list.begin(); it!=mutation_list.end();++it){
        PrintGenomeAux(it->second, true);
    }
}
/**
 * @brief Auxiliar function to print the genome.
 *
 * @param n Nucleotide pointer of the current position.
 * @param end Boolean to represent the end of nucleotide sequence.
 */
void Genome::PrintGenomeAux(Nucleotide* n, bool end){
    if(n){
        if (n->GetParent() && n->GetPosition() > 0)
            PrintGenomeAux(n->GetParent(), false);
        // Print value of the current nucleotide
        std::cout << int_to_base(n->GetValue());
        // If at the end print a new line
        if (end)
            std::cout << std::endl;
    }

    
}
/**
 * @brief Get the first nucleotide of the genome.
 *
 * @return Nucleotide pointer pointing to the first nucleotide of the genome.
 */
Nucleotide* Genome::GetHead(){
    return head;
}
/**
 * @brief Get the last nucleotide of the genome.
 *
 * @return Nucleotide pointer pointing to the last nucleotide of the genome.
 */
Nucleotide* Genome::GetTail(){
    return tail;
}
/**
 * @brief Get the nucleotide of the desired position.
 *
 * @param pos Integer contaning the desired position.
 * 
 * @return Nucleotide pointer of the desired position in the sequence.
 */
Nucleotide* Genome::GetPosition(int32_t pos){
    Nucleotide* it = tail;
    if(it->GetPosition() != pos)
        while(it != nullptr && it->GetPosition() > pos)
            it = it->GetParent();
    return it;

}
/**
 * @brief Free the memory used by the genome, its mutations and all the nucleotides.
 *
 * 
 */
Genome::~Genome()
{
    std::map<int, Nucleotide*>::iterator it;
    for(it = mutation_list.begin(); it != mutation_list.end(); ++it){
        std::cout << "Deleting mutation ID: " << it->first << std::endl;
        Nucleotide* n = it->second;
        while(n && !n->GetHasMutation()){
            Nucleotide* temp = n->GetParent();
            delete n;
            n = temp;
        }
    }
    Nucleotide* n = tail;
    while(n){
        Nucleotide* temp = n->GetParent();
        delete n;
        n = temp;
    }
}
/**
 * @brief Get the mutation rate
 * 
 * @return Mutation Rate 
 */
int Genome::GetMutationRate(){
    return mutation_rate;
}
/**
 * @brief Set the mutation rate
 * 
 * @param mutation_rate Mutation rate value
 */
void Genome::SetMutationRate(double mutation_rate){
    this->mutation_rate = mutation_rate;
}
/**
 * @brief Mutate a random position and base from a random previous mutation
 * 
 * @return Mutation index
 */
int Genome::MutationRandomPosition(){
	std::uniform_int_distribution pos_generator{0, genome_length-1};
	std::uniform_int_distribution base_generator{0, 3};
	std::uniform_int_distribution father_generator{0, mutation_number};
    int pos = pos_generator(mt);
    char base = int_to_base(base_generator(mt));
    int father = father_generator(mt);
    std::cout << "New Mutation: " << pos << " " << " " << base << " " << father << std::endl;
    return  MutatePosition(pos, base, father);


}