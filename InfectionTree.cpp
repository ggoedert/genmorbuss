#include "InfectionTree.hpp"

/**
 * @brief Vertex and edge structs used by boost to parse the graphml file.
 * 
 */
struct BoostVertex
{
    int id;
    double time_activated;
    double time_infectious;
    double time_exposed;
    double time_recovered;
    double vector_symptoms;
    int inf_placement;
    int age_group;
    int inf_source;
};
struct BoostEdge
{       
        std::string source;
        std::string target;
        double time;

};

/**
 * @brief Create a new infection tree
 * @param genome Genome pointer used by the infection tree.
*/
InfectionTree::InfectionTree(Genome* genome){
    this->genome = genome;
    this->root_id = -1;
}
/**
 * @brief Parses a graphml file using Boost library and add the elements to the tree's data structures
 * @param filename Filename of the .graphml file
*/
void InfectionTree::ReadGraphml(std::string filename){
    //creates boost properties
    using Graph =  boost::adjacency_list<boost::vecS, boost::vecS, boost::directedS, BoostVertex,BoostEdge>;
    Graph g;
    boost::dynamic_properties dp(boost::ignore_other_properties); //add custom properties
    dp.property("id", boost::get(&BoostVertex::id, g));
    dp.property("time_activated", boost::get(&BoostVertex::time_activated, g));
    dp.property("time_exposed", boost::get(&BoostVertex::time_exposed, g));
    dp.property("time_infectious", boost::get(&BoostVertex::time_infectious, g));
    dp.property("time_recovered", boost::get(&BoostVertex::time_recovered, g));
    dp.property("vector_symptoms", boost::get(&BoostVertex::vector_symptoms, g));
    dp.property("inf_placement", boost::get(&BoostVertex::inf_placement, g));
    dp.property("inf_source", boost::get(&BoostVertex::inf_source, g));
    dp.property("age_group", boost::get(&BoostVertex::age_group, g));
    dp.property("source", boost::get(&BoostEdge::source, g));
    dp.property("target", boost::get(&BoostEdge::target, g));
    dp.property("time", boost::get(&BoostEdge::time, g));
    //reads the file
    std::ifstream file(filename);
    boost::read_graphml(file,g,dp);
    //creates the vertex, in_degree and out_degree maps
    Graph::vertex_iterator v, vend;
    for (boost::tie(v, vend) = vertices(g); v != vend; ++v) {
        int id = g[*v].id;
        Vertex* v_tree = new Vertex(id);
        v_tree->SetTimeActivated(g[*v].time_activated);
        v_tree->SetTimeExposed(g[*v].time_exposed);
        v_tree->SetTimeInfectious(g[*v].time_infectious);
        v_tree->SetTimeRecovered(g[*v].time_recovered);
        v_tree->SetVectorSymptoms(g[*v].vector_symptoms);
        v_tree->SetInfPlacement(g[*v].inf_placement);
        v_tree->SetInfSource(g[*v].inf_source);
        v_tree->SetAgeGroup(g[*v].age_group);
        vertex_map.insert(std::make_pair(id, v_tree));
        out_degree_map.insert(std::make_pair(id, 0));
        in_degree_map.insert(std::make_pair(id, 0));
    }
    //iterates through the boost graphml edges and populates the edge, in_degree and out_degree maps
    auto es = boost::edges(g);
    std::map<int,int>::iterator in_degree_it;
    std::map<int,int>::iterator out_degree_it;
    for (auto eit = es.first; eit != es.second; ++eit) {
        int vertex_source = g[boost::source(*eit, g)].id;
        int vertex_target = g[boost::target(*eit, g)].id;
        in_degree_it = in_degree_map.find(vertex_target);
        out_degree_it = out_degree_map.find(vertex_source);
        if (in_degree_it != in_degree_map.end())
                in_degree_it->second++;
        if (out_degree_it != out_degree_map.end())
                out_degree_it->second++;
        std::map<int, Edge*>::iterator iter = edge_map.find(vertex_source);
        if (iter != edge_map.end() )
        {
            Edge* list = iter->second;
            Edge* e_it = list;
            while(e_it->GetNextEdge() != nullptr && e_it->GetConVertex()->GetId() != vertex_target){
                e_it = e_it->GetNextEdge();

            }
            if(e_it->GetNextEdge() == nullptr){
                Edge* e = new Edge(g[*eit].time, vertex_map.find(vertex_target)->second, nullptr);
                e_it->SetNextEdge(e);
            }
        }else{
            Edge* e = new Edge(g[*eit].time, vertex_map.find(vertex_target)->second, nullptr);
            edge_map.insert(std::make_pair(vertex_source, e));
        }
    }
    //checks which node has in_degree 0 and assigns its id to root_id
    for(in_degree_it = in_degree_map.begin(); in_degree_it != in_degree_map.end(); ++in_degree_it){
        if (in_degree_it->second == 0){
            root_id = in_degree_it->first;
        }

    }
}
/**
 * @brief Realize a depth first search in the tree.
 * 
 * @param initial_vertex Initial vertex Id of the search.
 * @param num_elements Integer used to store the number of elements resulting from the search.
 * @return Pointer to a Integer array containing the elements in order that they appear in the search.
 */
int* InfectionTree::DepthFirstSearch(int initial_vertex, int& num_elements){
    std::map<int,Edge*>::iterator it;
    std::stack<int> visited;
    std::stack<int> stk;
    // std::map<int,Edge*>::iterator it;
    int curr;
    stk.push(initial_vertex);
    while (!stk.empty()){
        curr = stk.top();
        stk.pop();
        visited.push(curr);
        it = edge_map.find(curr);
        if(it!= edge_map.end()){
            Edge* adj=it->second;
            while(adj){
                stk.push(adj->GetConVertex()->GetId());
                adj = adj->GetNextEdge();
            }
        }
    }
    num_elements = visited.size();
    if(num_elements > 0){
        int* elements = new int[num_elements];
        for(int i = 0; i < num_elements; i++){
            elements[i] = -1;
        }
        for(int i = num_elements-1; i>=0;i--){
            elements[i] = visited.top();
            visited.pop();
        }
        return elements;
    }
    else{
        return nullptr;
    }

}
/**
 * @brief Get the vertex's id assigned as root of the tree
 * 
 * @return The vertex if of the tree's root.
*/
int InfectionTree::GetRootId(){
    return root_id;
}

void InfectionTree::DoMutations(){
    std::map<int,Edge*>::iterator it;
    std::stack<int> visited;
    std::stack<int> stk;
    int curr;
    double curr_time = 0.0;
    double time_between_mut = 10;
    stk.push(GetRootId());
    while (!stk.empty()){
        curr = stk.top();
        Vertex* vet = vertex_map.find(curr)->second;
        //calculates the mutations
        for (int i = curr_time; i < vet->GetTimeInfectious(); i+=time_between_mut){
            std::uniform_real_distribution<double> dis(0.0, 1.0);
            if(genome->GetMutationRate() < dis(mt)){
                int mut_index = genome->MutationRandomPosition();
                vet->AddMutation(mut_index, i);
            }
        }
        curr_time = vet->GetTimeInfectious();
        stk.pop();
        visited.push(curr);
        it = edge_map.find(curr);
        if(it!= edge_map.end()){
            Edge* adj=it->second;
            while(adj){
                stk.push(adj->GetConVertex()->GetId());
                adj = adj->GetNextEdge();
            }
        }
    }
}

void InfectionTree::PrintTree(){
    int n_elements;
    int* elements = DepthFirstSearch(GetRootId(), n_elements);
    for(int i = 0; i < n_elements; i++){
        Vertex* vet = vertex_map.find(elements[i])->second;
        vet->PrintInfo();
    }
}