#ifndef INFECTIONTREE_HPP
#define INFECTIONTREE_HPP

#include <map>
#include <iostream>
#include <string>
#include <fstream>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/iteration_macros.hpp>
#include <boost/graph/graphml.hpp>
#include <boost/graph/graph_utility.hpp>
#include <boost/property_map/property_map.hpp>
#include <boost/property_map/dynamic_property_map.hpp>
#include <stack>
#include <random> // for std::mt19937
#include "Genome.hpp"

class Vertex {
    private:
        int id;
        double time_activated = 0;
        double time_infectious = 0;
        double time_exposed = 0;
        double time_recovered = 0;
        double vector_symptoms = 0;
        int inf_placement = 0;
        int age_group = 0;
        int inf_source = 0;
        std::vector< std::pair<int, double> > mutation_list;
        
        
    public:
        void PrintInfo(){
            std::cout << "-------------------------: " << id << " :------------------------- " << std::endl;
            std::cout << "time_activated: " << time_activated << std::endl;
            std::cout << "time_infectious: " << time_infectious << std::endl;
            std::cout << "time_exposed: " << time_exposed << std::endl;
            std::cout << "time_recovered: " << time_recovered << std::endl;
            std::cout << "vector_symptoms: " << vector_symptoms << std::endl;
            std::cout << "inf_placement: " << inf_placement << std::endl;
            std::cout << "age_group: " << age_group << std::endl;
            std::cout << "inf_source: " << inf_source << std::endl;
            std::cout << "Mutation list:" << std::endl;
            std::vector<std::pair<int, double> >::iterator it;
            for (it = mutation_list.begin(); it!= mutation_list.end(); ++it){
                std::cout << "\tMutation id: " << it->first << " Time: " << it->second << std::endl;
            }
        }
        Vertex(int id){
            this->id = id;
        };
        std::vector< std::pair<int, double> > GetMutationList(){
            return mutation_list;
        }
        void AddMutation(int mutation_id, double time){
            mutation_list.push_back(std::make_pair(mutation_id, time));
        }
        void SetTimeActivated(double time_activated){
            this->time_activated = time_activated;
        };
        void SetTimeInfectious(double time_infectious){
            this->time_infectious = time_infectious;
        };
        void SetTimeExposed(double time_exposed){
            this->time_exposed = time_exposed;
        };
        void SetTimeRecovered(double time_recovered){
            this->time_recovered = time_recovered;
        };
        void SetVectorSymptoms(double vector_symptoms){
            this->vector_symptoms = vector_symptoms;
        };
        void SetInfPlacement(int inf_placement){
            this->inf_placement = inf_placement;
        };
        void SetAgeGroup(int age_group){
            this->age_group = age_group;
        };
        void SetInfSource(int inf_source){
            this->inf_source = inf_source;
        };
        //------
        double GetTimeActivated(){
            return time_activated;
        };
        double GetTimeInfectious(){
            return time_infectious;
        };
        double GetTimeExposed(){
            return time_exposed;
        };
        double GetTimeRecovered(){
            return time_recovered;
        };
        double GetVectorSymptoms(){
            return vector_symptoms;
        };
        int GetInfPlacement(){
            return inf_placement;
        };
        int GetAgeGroup(){
            return age_group;
        };
        int GetInfSource(){
            return inf_source;
        };
        int GetId(){
            return id;
        };

};

class Edge {
    private:
        double time;
        Vertex* con_vertex = nullptr;
        Edge* next_edge = nullptr;
    public:
        Edge(double time, Vertex* con_vertex, Edge* next_edge){
            this->time = time;
            this->con_vertex = con_vertex;
            this->next_edge = next_edge;
        };
        void SetTime(double time){
            this->time = time;
        }; 
        void SetConVertex(Vertex* con_vertex){
            this->con_vertex = con_vertex;
        };
        void SetNextEdge(Edge* next_edge){
            this->next_edge = next_edge;
        };
        double GetTime(){
            return time;
        };
        Vertex* GetConVertex(){
            return con_vertex;
        };
        Edge* GetNextEdge(){
            return next_edge;
        };
};

class InfectionTree{
    private:
        std::map <int, Vertex*> vertex_map;
        std::map <int, Edge*> edge_map;
        std::map <int, int> in_degree_map;
        std::map <int, int> out_degree_map;
        std::mt19937 mt{};
        int root_id;
        bool directed = true;
        Genome* genome;

    public:
        InfectionTree(Genome* genome);
        int GetRootId();
        void ReadGraphml(std::string filename);
        int* DepthFirstSearch(int initial_vertex, int &num_elements);    
        void DoMutations();
        void PrintTree();
};

#endif
