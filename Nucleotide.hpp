#ifndef NUCLEOTIDE_HPP
#define NUCLEOTIDE_HPP

#include <iostream>

class Nucleotide
{
private:
    uint8_t value;
    int32_t position;
    bool has_mutation;
    Nucleotide *parent;
    Nucleotide *child[4] = {nullptr, nullptr, nullptr, nullptr};
public:
    Nucleotide(uint8_t value, Nucleotide* parent);
    void SetHasMutation(bool has_mutation);
    bool GetHasMutation();
    uint8_t GetValue();
    int32_t GetPosition();
    Nucleotide* GetParent();
    Nucleotide* GetChild(int index);
    Nucleotide** GetChildren();
    void SetParent(Nucleotide* parent);
    void SetPosition(int32_t position);
    void SetValue(uint8_t value);
    void SetChild(Nucleotide* child, int index);
    ~Nucleotide();
};



#endif
