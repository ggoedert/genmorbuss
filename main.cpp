#include <iostream>
#include "Genome.hpp"
#include "Nucleotide.hpp"
#include "InfectionTree.hpp"

int main(){
    char TEST_GENOME[] = {'C', 'G', 'T', 'A', 'G', '\0'};
    std::cout << "Testing genome (and nucleotide) class" << std::endl;
    Genome* genome1 = new Genome(TEST_GENOME);
    genome1->PrintGenome();
    // genome1->MutatePosition(4, 'C',0);
    // genome1->PrintGenome();
    // genome1->MutatePosition(2, 'A',1);
    // genome1->PrintGenome();
    std::cout << "Testing infection tree" << std::endl;
    InfectionTree* ift = new InfectionTree(genome1);
    ift->ReadGraphml("conf_2_seed_1_tree_0.graphml");
    int num_elements;
    ift->DoMutations();
    int* elements = ift->DepthFirstSearch(ift->GetRootId(), num_elements); 
    std::cout << "Depth first search" << std::endl;
    for(int i = 0; i < num_elements-1; i++){
        std::cout << elements[i] << ", ";
    }
    if(num_elements > 0)
        std::cout << elements[num_elements-1] << std::endl;
    ift->PrintTree();
    std::cout << "Test complete!" << std::endl;
    genome1->PrintGenome();
    delete genome1;
    delete ift;
    return 0;
}